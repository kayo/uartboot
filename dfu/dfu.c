#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#include "../src/bootloader.h"
#include "crc.h"

#ifndef BLOCK_SIZE
#define BLOCK_SIZE 768 //1024 /* 1 K bytes */
#endif

static content_t data[BUFFER_SIZE(BLOCK_SIZE)];
static request_t* request = (void*)data;
static response_t* response = (void*)data;

static void safe_put(int fd, const uint8_t *ptr, size_t len) {
  int bs;

  for(; len > 0; ){
    bs = write(fd, ptr, len);
    
    if(bs < 0){
      close(fd);
      error("ERROR writing to socket");
    }
    
    ptr += bs;
    len -= bs;
  }
}

static void safe_get(int fd, uint8_t *ptr, size_t len){
  int bs;

  for(; len > 0; ){
    bs = read(fd, ptr, len);
    
    if(bs < 0){
      close(fd);
      error("ERROR reading from socket");
    }
    
    ptr += bs;
    len -= bs;
  }
}

static void do_request(int fd) {
  size_t size = request_header_size(request) +
    request_payload_size(request);
  
  *(uint32_t*)(data + size) = crc_update(CRC_INIT, data, size);
  
  safe_put(fd, data, size + checksum_size());
  
  safe_get(fd, data, command_size());
  
  size = response_header_size(response);
  
  safe_get(fd, data + command_size(), size - command_size());
  
  safe_get(fd, data + size, response_payload_size(response) + checksum_size());

  size_t full = size + response_payload_size(response);
  
  if (crc_update(CRC_INIT, data, full) != *(const uint32_t*)(data + full)) {
    response->status = status_invalid;
  }
  
  switch (response->status) {
  case status_success: printf("ok\n"); break;
  case status_invalid: printf("invalid\n"); break;
  case status_writerr: printf("programming error\n"); break;
  case status_proterr: printf("write propected\n"); break;
  default: printf("error\n"); break;
  }
}

int main(int argc, char **argv) {
  int fwfd = 0, fwsize, sockfd = 0, portno, n;
  
  struct sockaddr_in serv_addr;
  struct hostent *server;
  
  if (argc < 4) {
    fprintf(stderr, "usage: %s <host> <port> <firmware>\n", argv[0]);
    exit(0);
  }
  
  struct stat st;
  stat(argv[3], &st);
  fwsize = st.st_size;
  
  fwfd = open(argv[3], O_RDONLY);
  
  if (fwfd < 0 || fwsize < 1) {
    fprintf(stderr, "Invalid firmmware or access denied\n");
    goto exit;
  }
  
  printf("Firmware: %s (%u bytes)\n", argv[3], fwsize);
  
  portno = atoi(argv[2]);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  
  if (sockfd < 0) {
    error("Unable to open socket");
  }
  
  server = gethostbyname(argv[1]);
  if (server == NULL) {
    fprintf(stderr, "Unable to resolve host\n");
    goto exit;
  }
  
  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy(server->h_addr,
        &serv_addr.sin_addr.s_addr,
        server->h_length);
  serv_addr.sin_port = htons(portno);
  
  if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    fprintf(stderr, "Unable to connect to target\n");
    goto exit;
  }
  
  crc_init();
  
  request->command = command_info;
  do_request(sockfd);
  
  if (response->status != status_success) {
    fprintf(stderr, "Unable to fetch bootloader info\n");
    goto exit;
  }
  
  address_t start_address = response->info.start_address;
  length_t block_maxsize = response->info.block_maxsize;
  
  printf("boot version: %u.%u.%u.%u\n"
         "start address: 0x%x\n"
         "block maxsize: %u\n",
         response->info.boot_version.major,
         response->info.boot_version.minor,
         response->info.boot_version.delta,
         response->info.boot_version.patch,
         response->info.start_address,
         response->info.block_maxsize);

  if (block_maxsize > BLOCK_SIZE) {
    block_maxsize = BLOCK_SIZE;
  }

  printf("Unlock target\n");
  
  request->command = command_unlock;
  request->unlock.address = address_none;
  request->unlock.length = 0;
  do_request(sockfd);

  if (response->status != status_success) {
    fprintf(stderr, "Unable to unlock target\n");
    goto exit;
  }
  
  request->command = command_erase;
  request->erase.address = start_address;
  request->erase.length = fwsize;

  printf("Erase 0x%08x (%u bytes)\n", request->erase.address, request->erase.length);
  
  do_request(sockfd);

  if (response->status != status_success) {
    fprintf(stderr, "Unable to erase pages\n");
    goto exit;
  }
  
  for ( ; fwsize > 0; ) {
    request->command = command_write;
    request->write.address = start_address;
    request->write.length = block_maxsize;
    
    if (request->write.length > fwsize)
      request->write.length = fwsize;
    
    start_address += request->write.length;
    fwsize -= request->write.length;
    
    safe_get(fwfd, request->write.content, request->write.length);
    
    printf("Program 0x%08x (%u bytes)\n", request->write.address, request->write.length);
    
    do_request(sockfd);
    
    if (response->status != status_success) {
      fprintf(stderr, "Unable to program page\n");
      goto exit;
    }
  }
  
  printf("Lock target\n");
  
  request->command = command_lock;
  request->lock.address = address_none;
  request->lock.length = 0;
  do_request(sockfd);
  
  if (response->status != status_success) {
    fprintf(stderr, "Unable to lock target\n");
    goto exit;
  }

  printf("Boot target\n");
  
  request->command = command_boot;
  do_request(sockfd);
  
  if (response->status != status_success) {
    fprintf(stderr, "Unable to boot firmware\n");
    goto exit;
  }

 exit:
  if (fwfd > 0) close(fwfd);
  if (sockfd > 0) close(sockfd);
  
  return 0;
}
