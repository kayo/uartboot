#ifndef __CRC_H__
#define __CRC_H__ "crc.h"

#define CRC_POLY   0x04c11db7
#define CRC_POLY_R 0xedb88320

#define CRC_INIT   0x00000000

void crc32_init(void);
uint32_t crc_update(uint32_t crc, const uint8_t *buf, size_t len);

#endif/*__CRC_H__*/
