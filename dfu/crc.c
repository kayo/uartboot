#include <arpa/inet.h>

#include "crc.h"

//static uint32_t crc_table[256];
static uint32_t crc_table_r[256];

void crc_init(void) {
  int i, j;
  uint32_t c, cr;
  for (i = 0; i < 256; ++i) {
    cr = i;
    c = i << 24;
    for (j = 8; j > 0; --j) {
      //c = c & 0x80000000 ? (c << 1) ^ CRC_POLY : (c << 1);
      cr = cr & 0x00000001 ? (cr >> 1) ^ CRC_POLY_R : (cr >> 1);
    }
    //crc_table[i] = c;
    crc_table_r[i] = cr;
  }
}

uint32_t crc_update(uint32_t crc, const uint8_t *ptr, size_t len){
  crc = ~crc;
  
  for (; len > 0; len --)
    crc = (crc >> 8) ^ crc_table_r[(crc ^ (*ptr++)) & 0xff];
  
  return ~crc;
}
