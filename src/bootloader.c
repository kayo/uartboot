#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/crc.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/systick.h>

#include <string.h>

#include "bootloader.h"

#ifndef PAGE_SIZE
#define PAGE_SIZE 1024
#endif

/* We have two phantom variabled, exported by ld,
   which points to start and end of application region in flash. */
extern const unsigned _app_start;
extern const unsigned _app_end;

/* Get addresses of application flash region. */
static const uint32_t app_start = (uint32_t)&_app_start;
static const uint32_t app_end = (uint32_t)&_app_end;

#ifndef APP_ADDRESS
#define APP_ADDRESS app_start
#endif

#ifndef USART_PORT
#define USART_PORT _USART1
#endif

#ifndef USART_GPIO
#define USART_GPIO _GPIOA
#endif

#ifndef USART_DMA
#define USART_DMA _DMA1
#endif

#ifndef USART_DMA_TX
#define USART_DMA_TX DMA1, DMA_CHANNEL4
#endif

#ifndef USART_DMA_TX_ISR
#define USART_DMA_TX_ISR dma1_channel4_isr
#endif

#ifndef USART_DMA_TX_CHN
#define USART_DMA_TX_CHN _CHANNEL4
#endif

#ifndef USART_DMA_RX
#define USART_DMA_RX DMA1, DMA_CHANNEL5
#endif

#ifndef BAUD_RATE
#define BAUD_RATE 38400
#endif

#ifndef BLOCK_SIZE
#define BLOCK_SIZE 1024 /* 1 K bytes */
#endif

#ifndef BOOT_DELAY
#define BOOT_DELAY 10 /* seconds */
#endif

#ifndef USE_SLEEP /* This mode may interfere with SWD debugging */
#define USE_SLEEP 0
#endif

#ifndef OPT_SIZE
#define OPT_SIZE 0
#endif

#define REG_USART1 USART1
#define REG_USART2 USART2
#define REG_USART3 USART3

#define CAT1(a) a
#define _CAT2(a, b) a ## b
#define CAT2(a, b) _CAT2(a, b)
#define _CAT3(a, b, c) a ## b ## c
#define CAT3(a, b, c) _CAT3(a, b, c)
#define _CAT4(a, b, c, d) a ## b ## c ## d
#define CAT4(a, b, c, d) _CAT4(a, b, c, d)

#define USART CAT2(REG, USART_PORT)
#define USART_DMA_TX_IRQ CAT4(NVIC, USART_DMA, USART_DMA_TX_CHN, _IRQ)

#if USE_SLEEP
static inline void wait_for_interrupt(void) {
  asm volatile("wfi");
}

static inline void enable_sleep_on_exit(void) {
  SCB_SCR |= SCB_SCR_SLEEPONEXIT;
}

static inline void disable_sleep_on_exit(void) {
  SCB_SCR &= ~SCB_SCR_SLEEPONEXIT;
}
#endif

static content_t data[BUFFER_SIZE(BLOCK_SIZE)];
static request_t* request = (void*)data;
static response_t* response = (void*)data;

static version_t boot_version = {BOOT_VERSION};

static int boot_count = BOOT_DELAY * 100;
static int boot_delta;

static inline void boot_now(void) {
  boot_count = 0;
}

static inline void boot_never(void) {
  boot_count = 1;
  boot_delta = 0;
}

static bool boot_valid(void) {
  return (MMIO32(APP_ADDRESS) & 0x2FFE0000) == 0x20000000;
}

typedef void boot_jump_t(void);

static inline void boot(void) {
  /* Set vector table base address. */
  SCB_VTOR = (APP_ADDRESS) & 0xFFFF;
  /* Initialise master stack pointer. */
  asm volatile("msr msp, %0"::"r" MMIO32(APP_ADDRESS));
  /* Jump to application. */
  ((boot_jump_t*)MMIO32((APP_ADDRESS) + 4))();
}

static void rx_enable(void) {
  /* Set RX data size in bytes */
  dma_set_number_of_data(USART_DMA_RX, sizeof(data));
  
  /* Immediately switch TX mode in order to receiving the next request */
  dma_enable_channel(USART_DMA_RX);
}

static void rx_disable(void) {
  /*dma_clear_interrupt_flags(USART_DMA_RX, DMA_GIF | DMA_TEIF | DMA_TCIF);*/
  dma_disable_channel(USART_DMA_RX);
}

static void tx_enable(uint16_t size) {
  dma_set_number_of_data(USART_DMA_TX, size);
  dma_enable_channel(USART_DMA_TX);
}

static void tx_disable(void) {
  /* Turn off TX mode */
  dma_disable_channel(USART_DMA_TX);
}

static inline void init(void) {
  rcc_clock_setup_in_hsi_out_24mhz();
  /* rcc_clock_setup_in_hse_8mhz_out_72mhz(); */
  
#if OPT_SIZE
  rcc_peripheral_enable_clock(&RCC_AHBENR, RCC_AHBENR_DMA1EN | RCC_AHBENR_CRCEN);
  rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPAEN | RCC_APB2ENR_USART1EN);
#else
  /* Enable clocks for GPIO port A (for GPIO_USART1_TX) and USART1. */
  rcc_periph_clock_enable(CAT2(RCC, USART_GPIO));
  rcc_periph_clock_enable(CAT2(RCC, USART_PORT));
  
  /* Enable DMA1 clock */
  rcc_periph_clock_enable(CAT2(RCC, USART_DMA));

  /* Enable CRC unit clock */
  rcc_periph_clock_enable(RCC_CRC);
#endif
  
  /* Setup GPIO pin GPIO_USART1_TX on GPIO port A for transmit. */
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, CAT3(GPIO, USART_PORT, _TX));
  
  /* Setup GPIO pin GPIO_USART1_RX on GPIO port A for receive. */
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, CAT3(GPIO, USART_PORT, _RX));

  /* Setup UART parameters. */
  usart_set_baudrate(USART, BAUD_RATE);
  usart_set_databits(USART, 8);
  usart_set_stopbits(USART, USART_STOPBITS_1);
  usart_set_parity(USART, USART_PARITY_NONE);
  usart_set_flow_control(USART, USART_FLOWCONTROL_NONE);
  usart_set_mode(USART, USART_MODE_TX_RX);

  /* TX transfer DMA */
  dma_channel_reset(USART_DMA_TX);
  dma_set_priority(USART_DMA_TX, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(USART_DMA_TX, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(USART_DMA_TX, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(USART_DMA_TX);
  dma_set_read_from_memory(USART_DMA_TX);
  dma_set_memory_address(USART_DMA_TX, (uint32_t)data);
  dma_set_peripheral_address(USART_DMA_TX, (uint32_t)&USART_DR(USART1));
  
  /* RX transfer DMA */
  dma_channel_reset(USART_DMA_RX);
  dma_set_priority(USART_DMA_RX, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(USART_DMA_RX, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(USART_DMA_RX, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(USART_DMA_RX);
  dma_enable_circular_mode(USART_DMA_RX);
  dma_set_read_from_peripheral(USART_DMA_RX);
  dma_set_peripheral_address(USART_DMA_RX, (uint32_t)&USART_DR(USART1));
  dma_set_memory_address(USART_DMA_RX, (uint32_t)data);
  
  /* Enable DMA TX interrupt */
  nvic_enable_irq(USART_DMA_TX_IRQ);
  
  /* Enable TX transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(USART_DMA_TX);
  
  /* Enable the USART DMA. */
  usart_enable_tx_dma(USART1);
  usart_enable_rx_dma(USART1);
  
  /* Enable the USART. */
  usart_enable(USART);
  rx_enable();
  
  /* Configure sys tick */
  /* 24MHz / 8 = 3e6 counts per second */
  systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
  /* 3e6 / 3e4 = 100 overflows per second - every 10ms one interrupt */
  systick_set_reload(3e4-1);
  
  /* Enable systick interrupt */
  nvic_enable_irq(NVIC_SYSTICK_IRQ);
  
  systick_interrupt_enable();
  systick_counter_enable();
}

static inline void done(void) {
  /* Disable sys tick counter for monitor step function */
  systick_counter_disable();
  systick_interrupt_disable();

  /* Disable systick interrupt */
  nvic_disable_irq(NVIC_SYSTICK_IRQ);
  
  /* Disable USART. */
  usart_disable(USART);
  
  /* Disable DMA TX interrupt */
  nvic_disable_irq(USART_DMA_TX_IRQ);
  
  /* Reset GPIO port configuration. */
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);

#if OPT_SIZE
  rcc_peripheral_disable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPAEN | RCC_APB2ENR_USART1EN);
  rcc_peripheral_disable_clock(&RCC_AHBENR, RCC_AHBENR_DMA1EN | RCC_AHBENR_CRCEN);
#else
  /* Disable CRC unit clock */
  rcc_periph_clock_disable(RCC_CRC);
  
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(CAT2(RCC, USART_DMA));
  
  /* Disable clocks for GPIO port A (for GPIO_USART1_TX) and USART1. */
  rcc_periph_clock_disable(CAT2(RCC, USART_PORT));
  rcc_periph_clock_disable(CAT2(RCC, USART_GPIO));
#endif
}

static inline uint32_t rbit32(uint32_t val) {
  asm volatile("rbit %0,%0":"+r" (val):"r" (val));
  return val;
}

static uint32_t crc_update(const content_t *ptr, length_t len) {
  //  uint32_t l = len / 4, e = len & 3;
  const uint32_t *buf = (const uint32_t*)ptr;
  uint32_t crc = 0, crc_ = CRC_DR;
  
  for (; len >= 4; len -= 4)
    crc_ = crc_calculate(rbit32(*buf++));
  
  crc = rbit32(crc_);
  
  if (len > 0) {
    /* reset crc data register (=0) */
    crc_calculate(crc_);
    
    length_t len8 = len << 3;
    crc = (crc >> len8) ^ rbit32( crc_calculate( rbit32( (*buf & ((1 << len8) - 1)) ^ crc ) >> (32 - len8) ) );
  }
  
  return ~crc;
}

static uint32_t status;

static bool validate_status(void) {
  status = flash_get_status_flags();
  flash_clear_status_flags();
  return status & (FLASH_SR_PGERR | FLASH_SR_WRPRTERR);
}

static inline void do_request(void) {
  switch (request->command) {
  case command_info:
    boot_never();
    response->info.boot_version = boot_version;
    response->info.start_address = APP_ADDRESS;
    response->info.block_maxsize = BLOCK_SIZE;
    goto success_end;
  case command_boot:
    if (boot_valid()) {
      boot_now();
      goto success_end;
    }
    goto invalid_end;
  case command_read:
    /* cmd adr len -> cmd sta len */
    memcpy(response->read.content, (const void*)&MMIO16(request->read.address), request->read.length);
    goto success_end;
  case command_lock:
    flash_lock();
    goto success_end;
  case command_unlock:
    flash_unlock();
    goto success_end;
  case command_erase:
    if(request->erase.address == address_none){
      flash_erase_all_pages();
      validate_status();
    }else{
#define address request->erase.address
      address_t end = address + request->erase.length;
      for (; address < end ;) {
        flash_erase_page(address);
        if(validate_status()){
          goto status_end;
        }
        address += PAGE_SIZE;
      }
#undef address
    }
    goto status_end;
  case command_write:
    if(request->write.length > 0){
      /* cmd adr len > cmd sta len */
#define address request->write.address
      address_t end = address + request->write.length;
      const content_t *content = request->write.content;
#define status_check()                          \
      if(validate_status()) {                   \
        response->write.length = end - address; \
        goto status_end;                        \
      }
      if(address & 0x1){ /* halfword edge */
        address --;
        uint16_t halfword = (((uint16_t)(*content) << 8) & 0xff00) | (MMIO16(address) & 0x00ff);
        flash_program_half_word(address, halfword);
        status_check();
        address ++;
        content ++;
      }
      for(; address <= end - 2; ){
        flash_program_half_word(address, *(const uint16_t*)content);
        status_check();
        address += 2;
        content += 2;
      }
      if(address == end - 1){
        uint16_t halfword = (uint16_t)(*content) & 0x00ff;
        flash_program_half_word(address, halfword);
        status_check();
        address ++;
        content ++;
      }
      
      response->write.length = end - address;
      goto status_end;
#undef address
    }
    
    goto success_end;
  }

 success_end:
  response->status = status_success;
  return;

 invalid_end:
  response->status = status_invalid;
  return;

 status_end:
  response->status = status & FLASH_SR_PGERR ? status_writerr : status & FLASH_SR_WRPRTERR ? status_proterr : status_success;
  return;
}

void USART_DMA_TX_ISR(void) {
  if (dma_get_interrupt_flag(USART_DMA_TX, DMA_TCIF)) { /* transfer complete */
    tx_disable();
    rx_enable();
  }
  dma_clear_interrupt_flags(USART_DMA_TX, DMA_GIF | DMA_TEIF | DMA_TCIF);
}

static inline uint32_t dma_is_active(uint32_t dma, uint8_t channel) {
  return DMA_CCR(dma, channel) & DMA_CCR_EN;
}

static inline uint32_t dma_count_data(uint32_t dma, uint8_t channel) {
  return DMA_CNDTR(dma, channel);
}
  
static inline void handle_io(void) {
  length_t size = sizeof(data) - dma_count_data(USART_DMA_RX);

  /* Check the receiver is active and line idle */
  if (!(dma_is_active(USART_DMA_RX) &&
        usart_get_flag(USART1, USART_SR_IDLE)))
    return;
  
  /* Check the errors and data consistency */
  if (usart_get_flag(USART1, USART_SR_NE | USART_SR_FE) ||
      size < command_size() ||
      size < request_header_size(request) ||
      size < request_header_size(request) +
      request_payload_size(request) +
      checksum_size()) {
    /*rx_disable();*/
    rx_enable();
    return;
  }
  
  /* request received */
  rx_disable();

  size -= checksum_size();
  
  crc_reset();
  
  if (crc_update(data, size) == *(const uint32_t*)(data + size)) {
    do_request();
  } else {
    response->command = command_none;
    response->status = status_invalid;
  }
  
  size = response_header_size(response) + response_payload_size(response);
  
  crc_reset();
  *(uint32_t*)(data + size) = crc_update(data, size);

  tx_enable(size + checksum_size());
}

#if !USE_SLEEP
static volatile bool tick_fired;
#endif

void sys_tick_handler(void) {
  /* Wake-up monitor (Main thread) */
#if USE_SLEEP
  disable_sleep_on_exit();
#else
  tick_fired = true;
#endif
}

static void wait_systick(void) {
#if USE_SLEEP
  enable_sleep_on_exit();
  wait_for_interrupt();
#else
  tick_fired = false;
  for ( ; !tick_fired; )
    asm("nop");
#endif
}

int main(void) {
  init();
  
  /* when app is invalid then boot never */
  boot_delta = boot_valid() ? 1 : 0;
  
  for ( ; boot_count > 0 || dma_is_active(USART_DMA_TX); boot_count -= boot_delta) {
    wait_systick();
    handle_io();
  }
  
  done();
  
  boot();
  
  return 0;
}
