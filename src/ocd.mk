OCD_INTERFACE ?= jlink
OCD_TRANSPORT ?= swd
OCD_TARGET    ?= stm32f1x

OCD_COMMAND = -openocd \
  -c "source [find interface/$(OCD_INTERFACE).cfg]" \
  $(if $(OCD_TRANSPORT),-c "transport select $(OCD_TRANSPORT)") \
  -c "source [find target/$(OCD_TARGET).cfg]"


options:
	$(OCD_COMMAND) -c "init" -c "reset" -c "halt" -c "$(OCD_TARGET) options_read 0" -c "shutdown"

protect:
	$(OCD_COMMAND) -c "init" -c "halt" -c "flash protect 0 0 last on" -c "reset run" -c "shutdown"

unprotect:
	$(OCD_COMMAND) -c "init" -c "halt" -c "flash protect 0 0 last off" -c "reset run" -c "shutdown"

program: $(BINARY).elf
	$(OCD_COMMAND) -c "init" -c "program $< verify reset" -c "shutdown"

restart:
	$(OCD_COMMAND) -c "init" -c "reset init" -c "reset run" -c "shutdown"

debug:
	$(OCD_COMMAND) -c "init" -c "reset init"

attach:
	$(OCD_COMMAND)
