#ifndef __BOOTLOADER_H__
#define __BOOTLOADER_H__ "bootloader.h"

#include <stdint.h>

#ifndef BOOT_VERSION
#define BOOT_VERSION 0,1,0,1
#endif

typedef uint32_t command_t;
typedef uint32_t address_t;
typedef uint32_t length_t;
typedef uint32_t checksum_t;
typedef uint32_t status_t;
typedef uint8_t content_t;

const address_t address_none = -1;

enum command {
  command_none = '_',
  command_info = 'i',
  command_read = 'r',
  command_lock = 'l',
  command_unlock = 'u',
  command_erase = 'e',
  command_write = 'w',
  command_check = 'c',
  command_boot = 'b',
};

typedef struct {
  command_t command;
  union {
    struct {
      content_t end[0];
    } info, boot;
    struct {
      address_t address;
      length_t length;
      content_t end[0];
    } read, check, lock, unlock, erase;
    struct {
      address_t address;
      length_t length;
      content_t end[0], content[0];
    } write;
  };
} request_t;

#define command_size() sizeof(command_t)
#define checksum_size() sizeof(checksum_t)

static inline length_t request_header_size(const request_t *request) {
  switch (request->command) {
#define _(x) case command_##x: return (length_t)(((request_t*)NULL)->x.end-(content_t*)NULL)
    _(info);
    _(lock);
    _(unlock);
    _(erase);
    _(read);
    _(check);
    _(write);
    _(boot);
#undef _
  default: return 0;
  }
}

static inline length_t request_payload_size(const request_t *request) {
  return request->command != command_write ? 0 : request->write.length;
}

enum status {
  status_success = 'o',
  status_invalid = 'i',
  status_writerr = 'w',
  status_proterr = 'p',
};

typedef struct {
  uint8_t major;
  uint8_t minor;
  uint8_t delta;
  uint8_t patch;
} version_t;

typedef struct {
  command_t command;
  status_t status;
  union {
    struct {
      version_t boot_version;
      address_t start_address;
      length_t block_maxsize;
      content_t end[0];
    } info;
    struct {
      content_t end[0];
    } lock, unlock, erase, boot, none;
    struct {
      length_t length;
      content_t end[0], content[0];
    } read;
    struct {
      checksum_t checksum;
      content_t end[0];
    } check;
    struct {
      length_t length;
      content_t end[0];
    } write;
  };
} response_t;

static inline length_t response_header_size(const response_t *response) {
  switch (response->command) {
#define _(x) case command_##x: return (length_t)(((response_t*)NULL)->x.end-(content_t*)NULL)
    _(none);
    _(info);
    _(lock);
    _(unlock);
    _(erase);
    _(read);
    _(check);
    _(write);
    _(boot);
#undef _
  default: return 0;
  }
}

static inline length_t response_payload_size(const response_t *response) {
  return response->command != command_read ? 0 : response->read.length;
}

#define BUFFER_SIZE(payload) (sizeof(request_t) + payload + sizeof(checksum_t))

#endif/*__BOOTLOADER_H__*/
